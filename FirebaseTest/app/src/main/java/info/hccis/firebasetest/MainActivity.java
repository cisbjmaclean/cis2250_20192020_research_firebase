package info.hccis.firebasetest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    // Reference to the document in the firestore. Could also use the following way
    // db.collection("test").document("message");
    private DocumentReference docRef = db.document("Inbox/Sent");
    private CollectionReference colRef = db.collection("Inbox");
    private TextView messageText, subjectText, displayText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Get the views and button.
        displayText = findViewById(R.id.displayText);
        messageText = findViewById(R.id.messageText);
        subjectText = findViewById(R.id.subjectText);
        Button saveButton = findViewById(R.id.save_button);
        // Set the on click listener.
        saveButton.setOnClickListener(this);
//        saveButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Fields in firestore are stored in key value pairs. so to update the message document
//                // you need to make a message hashmap to store the values. Then when you want to set or update
//                // You can pass the hashmap.
//                Map<String, Object> message = new HashMap<>();
//                message.put("title", editText.getText().toString());
//
//                // If the document didn't have a title field this would add it to the document.
//                // Using set will also overwrite the document so if there were fields left out in the new
//                // object then they will be gone in the firestore.
//                ref.set(message).addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        Log.d("Cody","Saved");
//                    }
//                }).addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.d("Cody","Couldn't save");
//                    }
//                });
//                // Update will update the document based on the field named or by passing in a hashmap.
//                // Update can be used to add new fields to already existing documents.
//                ref.update("title", editText.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        Log.d("Cody","Saved");
//                    }
//                }).addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.d("Cody","Couldn't save");
//                    }
//                });
//            }
//        });
//    }
    }
    @Override
    protected void onStart() {
        super.onStart();
        // This will set a listener to the document so that the app will get live updates from the firestore.
        colRef.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null){
                    Toast.makeText(MainActivity.this, "Database  " + FirebaseApp.getInstance().getOptions().getProjectId() + " updated", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(MainActivity.this, "Not connected to Firestore" , Toast.LENGTH_LONG).show();
                }
                // This for loop grabs the data and displays it
                String data = "";
                for(QueryDocumentSnapshot snapshots : queryDocumentSnapshots){
                    Email email = snapshots.toObject(Email.class);
                    data += "Subject: " + snapshots.getId() + "\nMessage: " + email.getMessage() + "\n\n";
                }
                displayText.setText(data);
            }
        });
    }
    // This switch statement uses on click listeners for the following buttons
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_button:
                saveMessage();
                break;
//            case R.id.delete_message:
//                deleteMessages();
//                break;
//            case R.id.show_message:
//                getMessages();
//                break;
//            case R.id.update_message:
//                updateMessage();
//                break;
        }
    }
    // Saves data to Firestore
    private void saveMessage() {
        String message = messageText.getText().toString();
        String subject = subjectText.getText().toString();
        Email email = new Email(message);
        colRef.document(subject).set(email);
    }
}