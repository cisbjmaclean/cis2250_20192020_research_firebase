package info.hccis.firebasetest;

class Email {
    String message;
    String subject;

    public Email() {
    }

    public Email(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
